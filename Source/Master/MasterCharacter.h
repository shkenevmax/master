// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "DefaultTypes.h"
#include "GameFramework/Character.h"
#include "MasterCharacter.generated.h"

class AMasterPlayerController;
class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;

UCLASS(config=Game)
class AMasterCharacter : public ACharacter
{
	GENERATED_BODY()

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		UCameraComponent* FirstPersonCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		UArrowComponent* FPArrowCameraComponent;

public:
	AMasterCharacter();

protected:
	virtual void BeginPlay();
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		USkeletalMeshComponent* Mesh1P;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		USkeletalMeshComponent* Mesh3P;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
		float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UMasterInventoryComp* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UMasterCharHealthComp* CharHealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		AMasterPlayerController* MyPlayerController = nullptr;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
		FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
		TSubclassOf<class AMasterProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
		USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		UAnimMontage* FireAnimation;
		
	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		uint8 bUsingMotionControllers : 1;

protected:
	
	/** Fires a projectile. */
	void OnFire();
	void OnFireStop();

	//SwichWeapon
	UFUNCTION(Server, Reliable)
		void TrySwichNextWeapon();
	UFUNCTION(Server, Reliable)
		void TrySwichPreviosWeapon();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	UPROPERTY(Replicated)
		AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(Replicated)
		AWeaponDefault* CurrentWeapon3P = nullptr;
	UPROPERTY(Replicated)
		int32 CurrentIndexWeapon = 0;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadAnims;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		UAnimMontage* StunAnim;
	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* Anim);
		FTimerHandle RagdollTimer;
		FName GetWeaponAttachPoint() const;
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		FName WeaponAttachPoint;
		USkeletalMeshComponent* GetSpecifcPawnMesh(bool WantFirstPerson) const;
	UFUNCTION(BlueprintCallable, Category = Mesh)
		virtual bool IsFirstPerson() const;
		USkeletalMeshComponent* GetPawnMesh() const;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire")
		FVector CharForwardVector = FVector(0);

	// Weapon func
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* FPAnim, UAnimMontage* TPAnim);
		// void WeaponReloadStart(UAnimMontage* Anim, AActor* MagazineDrop); delete?
	UFUNCTION()
		void WeaponFire(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFire_BP(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* FPAnim, UAnimMontage* TPAnim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeapon, FAmmoInWeapon WeaponAdditionalInfo, int32 NewCurrentIndexWeapon, FAmmoSlot AmmoSlotInfo);
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();
	UFUNCTION(Server, Reliable)
		void TryReloadWeapon_OnServer();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		FRotator GetAimOffsets() const;

	//Weapon var
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
		bool IsFireing = false;

	// Movement flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement flags")
		bool SprintRunEnabled = false; // todo?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement flags")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement flags")
		bool WalkEnabled = false;

	// Dead event
	UFUNCTION()
		void CharDead(AController* DamageInstigator);
	UFUNCTION(NetMulticast, Reliable)
		void EnableRagdoll_Multicast();
	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP(AController* DamageInstigator);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetAliveStatus();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};

