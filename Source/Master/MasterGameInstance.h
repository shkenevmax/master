// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "DefaultTypes.h"
#include "WeaponDefault.h"
#include "MasterGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MASTER_API UMasterGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	// Property
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		UDataTable* WeaponInfoTable = nullptr;

	// Functions
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
