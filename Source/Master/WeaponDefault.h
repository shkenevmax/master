// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MasterCharacter.h"
#include "Components/ArrowComponent.h"
#include "DefaultTypes.h"
#include "MasterProjectile.h"
#include "Net/UnrealNetwork.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadStart, UAnimMontage*, FPAnim, UAnimMontage*, TPAnim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);

UCLASS()
class MASTER_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponFireStart OnWeaponFireStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon1P = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon3P = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
		FWeaponInfo WeaponSetting;
	UPROPERTY()
		FName IdWeaponName = WeaponSetting.WeaponName;
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Weapon Info")
		FAmmoInWeapon AmmoInWeapon;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Tick logic
	void FireTick(float DeltaTime); // ��������� ����������� �������� ������ ���
	void ReloadTick(float DeltaTime); // ��������� ����������� �������������� ������ ���

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowFireDebug = false;

	// weapon flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponFiring = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponReloading = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponAiming = false;

		bool BlockFire = false;

		// reloading func
		bool CanWeaponReload();
		void InitReload();
		void FinishReload();
		void CancelReload();

		void WeaponInit();

	// fire func
	UFUNCTION(Server, Reliable)
		void Fire_OnServer();
		bool CheckWeaponCanFire();
	UFUNCTION(NetMulticast, Unreliable)
		void AnimWeaponStart_Multicast(UAnimMontage* Anim);
		FVector GetFireEndLocation() const;
	UFUNCTION(Server, Reliable, BlueprintCallable)
		void SetWeaponStateFire_OnServer(bool bIsFire);
		bool CanWeaponFire();
	//fire var
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire logic")
		float SizeVectorToChangeShootDirection = 100.0f;
	UPROPERTY(Replicated)
		FVector ShootEndLocation = FVector(0);

	//Timers'flags
		float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
		bool DropShellFlag = false;
		float DropShellTimer = -1.0f;
		bool DropClipFlag = false;
		float DropClipTimer = -1.0f;

	// dispersion func
		void ChangeDispersionByShot();
		float GetCurrentDispersion() const;
		FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	// dispersion var
	UPROPERTY(Replicated)
		bool ShouldReduceDispersion = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float CurrentDispersion = 0;
		float CurrentDispersionMax = 0.3f;
		float CurrentDispersionMin = 0.1f;
		float CurrentDispersionRecoil = 0.1f;
		float CurrentDispersionReduction = 0.1f;

	// drop mesh
	UFUNCTION(Server, Reliable)
		void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, 
				float LifeTimeMesh, float ImpulseRundomDirection, float PowerImpulse, float CustomMass);
		void ClipDropTick(float DeltaTime);
		void ShellDropTick(float DeltaTime);
	UFUNCTION(NetMulticast, Unreliable)
		void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
		float LifeTimeMesh, float ImpulseRundomDirection, float PowerImpulse, float CustomMass, FVector LocalDir);

	// projectile info
	int8 GetNumberProjectileByShoot() const;
	UPROPERTY()
		int NumberProjectile = 1;
	FProjectileInfo GetProjectile();
	// trace
	UFUNCTION(NetMulticast, Unreliable)
		void TraceShowDebug_Multicast(FVector SpawnLocation, UArrowComponent* ShootLoc, float DistanceTrace);
	UFUNCTION(NetMulticast, Unreliable)
		void SpawnTraceDecal_Multicast(UMaterialInterface* HitMaterial, FHitResult Hit);
	UFUNCTION(NetMulticast, Unreliable)
		void SpawnTraceParticle_Multicast(UParticleSystem* HitParticle, FHitResult Hit);
	UFUNCTION(NetMulticast, Unreliable)
		void SpawnTraceHitSound_Multicast(USoundBase* HitSound, FHitResult Hit);

	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound(); // �������� ����� �������� � ������

	UFUNCTION(NetMulticast, Unreliable)
		void FXWeaponFire_Multicast(UParticleSystem* FxFire, USoundBase* SoundFire); // ������� ����� � ������ ��������

	int8 GetAvailableAmmoForReload(); // �������� ��������� ���������� �������� ��� ����������� �� ���������

	// owning pawn
	UPROPERTY(Transient, ReplicatedUsing = OnRep_MyPawn)
		class AMasterCharacter* MyPawn;

		void SetOwningPawn(AMasterCharacter* NewOwner);
		void DetachMeshFromPawn();
		void AttachMeshToPawn();
		USkeletalMeshComponent* GetWeaponMesh() const;
	UFUNCTION()
		void OnRep_MyPawn();
		virtual void OnEnterInventory(AMasterCharacter* NewOwner);
		virtual void OnLeaveInventory();
};
