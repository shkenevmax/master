// Copyright Epic Games, Inc. All Rights Reserved.

#include "MasterCharacter.h"
#include "MasterProjectile.h"
#include "MasterPlayerController.h"
#include "MasterGameMode.h"
#include "MasterGameInstance.h"
#include "WeaponDefault.h"
#include "MasterInventoryComp.h"
#include "MasterCharHealthComp.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AMasterCharacter

AMasterCharacter::AMasterCharacter()
{
	bReplicates = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a ArrowCameraComponent
	FPArrowCameraComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("FirstPersonCameraArrow"));
	FPArrowCameraComponent->SetupAttachment(FirstPersonCameraComponent);

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh3P"));
	Mesh3P->SetOnlyOwnerSee(false);
	Mesh3P->SetupAttachment(FirstPersonCameraComponent);
	Mesh3P->bCastDynamicShadow = false;
	Mesh3P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh3P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	InventoryComponent = CreateDefaultSubobject<UMasterInventoryComp>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UMasterCharHealthComp>(TEXT("CharHealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &AMasterCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AMasterCharacter::InitWeapon);
	}
}

void AMasterCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();


}

bool AMasterCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	return false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMasterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMasterCharacter::OnFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AMasterCharacter::OnFireStop);

	// Reload event
	PlayerInputComponent->BindAction("ReloadEvent", IE_Released, this, &AMasterCharacter::TryReloadWeapon);

	// Switch weapon event
	PlayerInputComponent->BindAction("SwitchNextWeapon", IE_Pressed, this, &AMasterCharacter::TrySwichNextWeapon);
	PlayerInputComponent->BindAction("SwitchPreviosWeapon", IE_Released, this, &AMasterCharacter::TrySwichPreviosWeapon);

	// Enable touchscreen input
	// EnableTouchscreenMovement(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AMasterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMasterCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMasterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMasterCharacter::LookUpAtRate);
}

void AMasterCharacter::OnFire()
{
	// if (CharHealthComponent && CharHealthComponent->GetIsAlive())
		AttackCharEvent(true);
}

void AMasterCharacter::OnFireStop()
{
	AttackCharEvent(false);
}

void AMasterCharacter::TrySwichNextWeapon_Implementation()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAmmoInWeapon OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AmmoInWeapon;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("There is NO current weapon =("));
		}
		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaponToIndexNOP(CurrentIndexWeapon + 1, OldIndex, OldInfo, true);
		}
	}
}

void AMasterCharacter::TrySwichPreviosWeapon_Implementation()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAmmoInWeapon OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AmmoInWeapon;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaponToIndexNOP(CurrentIndexWeapon - 1, OldIndex, OldInfo, false);
		}
	}
}

void AMasterCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AMasterCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AMasterCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMasterCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMasterCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (Mesh3P && Mesh3P->GetAnimInstance())
	{
		Mesh3P->GetAnimInstance()->Montage_Play(Anim, 0.5f);
	}
}

FName AMasterCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

USkeletalMeshComponent* AMasterCharacter::GetSpecifcPawnMesh(bool WantFirstPerson) const
{
	return WantFirstPerson == true ? Mesh1P : Mesh3P;
}

bool AMasterCharacter::IsFirstPerson() const
{
	return (Controller && Controller->IsLocalPlayerController());
}

USkeletalMeshComponent* AMasterCharacter::GetPawnMesh() const
{
	return IsFirstPerson() ? Mesh1P : Mesh3P;
}

void AMasterCharacter::TryReloadWeapon()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}
	WeaponReloadStart(CurrentWeapon->WeaponSetting.AnimationWeaponInfo.FPAnimCharReload, CurrentWeapon->WeaponSetting.AnimationWeaponInfo.TPAnimCharReload);
}

void AMasterCharacter::WeaponReloadStart(UAnimMontage* FPAnim, UAnimMontage* TPAnim)
{
	WeaponReloadStart_BP(FPAnim, TPAnim);
}

void AMasterCharacter::WeaponFire(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAmmoInWeaponInfo(CurrentIndexWeapon, CurrentWeapon->AmmoInWeapon);
	}
	WeaponFire_BP(Anim);
}

void AMasterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeAmmo(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		InventoryComponent->SetAmmoInWeaponInfo(CurrentIndexWeapon, CurrentWeapon->AmmoInWeapon);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void AMasterCharacter::WeaponFire_BP_Implementation(UAnimMontage* Anim) {}

void AMasterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* FPAnim, UAnimMontage* TPAnim) {}

void AMasterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess) {}

void AMasterCharacter::InitWeapon(FName IdWeapon, FAmmoInWeapon AmmoInWeaponInfo, int32 NewCurrentIndexWeapon, FAmmoSlot AmmoSlotInfo)
{
	// On server

	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UMasterGameInstance* masterGI = Cast<UMasterGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (masterGI)
	{
		if (masterGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetController();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* spawnedWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (spawnedWeapon)
				{
					FAttachmentTransformRules attachRule(EAttachmentRule::SnapToTarget, false);
					WeaponAttachPoint = FName("WeaponSocketRightHand");
					spawnedWeapon->SetOwningPawn(this);

					if (IsLocallyControlled())
					{
						spawnedWeapon->AttachToComponent(Mesh3P, attachRule, FName("WeaponSocketRightHand"));
					}
					else
					{
						spawnedWeapon->AttachToComponent(Mesh1P, attachRule, FName("WeaponSocketRightHand"));
					}
						
					spawnedWeapon->AttachMeshToPawn();

					CurrentWeapon = spawnedWeapon;

					spawnedWeapon->WeaponSetting = myWeaponInfo;

					// spawnedWeapon->UpdateStateWeapon_OnServer(MovementState);

					//debug remove
					spawnedWeapon->ReloadTimer = myWeaponInfo.ReloadTime;

					spawnedWeapon->AmmoInWeapon = AmmoInWeaponInfo;

					if (InventoryComponent) CurrentIndexWeapon = NewCurrentIndexWeapon;

					spawnedWeapon->OnWeaponReloadStart.AddDynamic(this, &AMasterCharacter::WeaponReloadStart);
					spawnedWeapon->OnWeaponReloadEnd.AddDynamic(this, &AMasterCharacter::WeaponReloadEnd);
					spawnedWeapon->OnWeaponFireStart.AddDynamic(this, &AMasterCharacter::WeaponFire);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if (InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAvalible.Broadcast(spawnedWeapon->WeaponSetting.WeaponType);
					}
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("AMasterCharacter::InitWeapon - weapon not found in table - NULL"));
			}
		}
	}
}

void AMasterCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);

		IsFireing = true;

		if (myWeapon->WeaponFiring)
		{
			if (AimEnabled)
			{
				PlayAnimMontage(myWeapon->WeaponSetting.AnimationWeaponInfo.AnimCharFireAiming);
			}
			else
			{
				// WeaponFire(myWeapon->WeaponSetting.AnimationWeaponInfo.AnimCharFire);
			}
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon - NULL"));
}

AWeaponDefault* AMasterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

int32 AMasterCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

FRotator AMasterCharacter::GetAimOffsets() const
{
	const FVector AimDirWS = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}

void AMasterCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CanWeaponReload())
	{
		CurrentWeapon->InitReload();
	}
}

void AMasterCharacter::CharDead(AController* DamageInstigator)
{
	CharDead_BP(DamageInstigator);

	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadAnims.Num());

		if (DeadAnims.IsValidIndex(rnd) && DeadAnims[rnd] && Mesh3P && Mesh3P->GetAnimInstance())
		{
			TimeAnim = DeadAnims[rnd]->GetPlayLength();
			PlayAnim_Multicast(DeadAnims[rnd]);
		}

		// Timer ragdoll
		GetWorldTimerManager().SetTimer(RagdollTimer, this, &AMasterCharacter::EnableRagdoll_Multicast, TimeAnim, false);

		SetLifeSpan(2.1f);

		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(2.1f);
		}

		if (MyPlayerController)
		{
			MyPlayerController->UnPossess();
			AMasterGameMode* MasterGameMode = Cast<AMasterGameMode>(UGameplayStatics::GetGameMode(this));
			MasterGameMode->TryToRespawnPlayer(MyPlayerController);
		}
	}
	else
	{
		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}
}

bool AMasterCharacter::GetAliveStatus()
{
	bool result = false;
	if (CharHealthComponent)
	{
		result = CharHealthComponent->GetIsAlive();
	}
	return result;
}

float AMasterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		CharHealthComponent->ChangeCurrentHealth_OnServer(-DamageAmount, EventInstigator);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AMasterProjectile* myProjectile = Cast<AMasterProjectile>(DamageCauser);
		if (myProjectile)
		{
			//UDefaultTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType()); // NAME_None - bone for radial damage
		}
	}

	return ActualDamage;
}

void AMasterCharacter::CharDead_BP_Implementation(AController* DamageInstigator) { /*BP only*/ }

void AMasterCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (Mesh3P)
	{
		Mesh3P->SetCollisionObjectType(ECC_PhysicsBody);
		Mesh3P->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
		Mesh3P->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		Mesh3P->SetSimulatePhysics(true);
	}
	GetWorld()->GetTimerManager().ClearTimer(RagdollTimer);
}

void AMasterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMasterCharacter, MovementState);
	DOREPLIFETIME(AMasterCharacter, CurrentWeapon);
	
	/*DOREPLIFETIME(ATopDownShooterCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATopDownShooterCharacter, Effects);
	DOREPLIFETIME(ATopDownShooterCharacter, EffectAdd);
	DOREPLIFETIME(ATopDownShooterCharacter, EffectRemove);*/
}