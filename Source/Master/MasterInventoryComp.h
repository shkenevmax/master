// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DefaultTypes.h"
#include "Components/ActorComponent.h"
#include "MasterInventoryComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnSwitchWeapon, FName, WeaponIdName, FAmmoInWeapon, AmmoInWeaponInfo, int32, NewCurrentIndexWeapon, FAmmoSlot, AmmoSlotInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAmmoInWeapon, AmmoInWeaponInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvalible, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChanged, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveNotRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MASTER_API UMasterInventoryComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMasterInventoryComp();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnSwitchWeapon OnSwitchWeapon;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoChange OnAmmoChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAmmoAvalible OnWeaponAmmoAvalible;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnUpdateWeaponSlots OnUpdateWeaponSlots;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponHaveNotRound OnWeaponHaveNotRound;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponHaveRound OnWeaponHaveRound;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		int32 MaxSlotWeapon = 0;

	bool SwitchWeaponToIndexNOP(int32 ChangeToIndex, int32 OldIndex, FAmmoInWeapon OldInfo, bool bIsForward);
	void SetAmmoInWeaponInfo(int32 IndexWeapon, FAmmoInWeapon NewInfo);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool ChekCanTakeAmmo(EWeaponType AmmoType);

	// net func
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inv")
		void WeaponAmmoEmptyEvent_Multicast(EWeaponType TypeWeapon);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inv")
		void WeaponAmmoAvalibleEvent_Multicast(EWeaponType TypeWeapon);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inv")
		void WeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAmmoInWeapon AdditionalInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inv")
		void SwitchWeaponEvent_OnServer(FName WeapomName, FAmmoInWeapon AdditionalInfo, int32 IndexSlot, FAmmoSlot AmmoSlotInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inv")
		void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inv")
		void AmmoChangeEvent_Multicast(EWeaponType TypeWeapon, int32 Count);


		bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvailableAmmoForWeapon);
	UFUNCTION(BlueprintCallable)
		void AmmoSlotChangeAmmo(EWeaponType WeaponType, int32 CoutChangeAmmo);
};
