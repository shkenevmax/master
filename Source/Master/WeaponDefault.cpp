// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "MasterCharacter.h"
#include "MasterInventoryComp.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Net/UnrealNetwork.h"
#include "MasterProjectile.h"
#include "DefaultTypes.h"
#include "Engine/StaticMeshActor.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh 1P"));
	SkeletalMeshWeapon1P->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon1P->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon1P->SetupAttachment(RootComponent);

	SkeletalMeshWeapon3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh 3P"));
	SkeletalMeshWeapon3P->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon3P->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon3P->SetupAttachment(SkeletalMeshWeapon1P);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(SkeletalMeshWeapon1P);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);

	ShootEndLocation = ShootLocation->GetForwardVector() + 100.0f;
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.0f)
		{
			Fire_OnServer();
		}
		else
			FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AmmoInWeapon.Round;
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int8 AvalibleAmmoFromInventory = GetAvailableAmmoForReload();

	if (AvalibleAmmoFromInventory > WeaponSetting.MaxRound)
	{
		AvalibleAmmoFromInventory = WeaponSetting.MaxRound;
	}

	// �������� ������

	int32 AmmoTake = AmmoInWeapon.Round;
	int32 AmmoNeedTake = 0;
	AmmoNeedTake = AmmoTake - AvalibleAmmoFromInventory;
	AmmoInWeapon.Round = AvalibleAmmoFromInventory;

	OnWeaponReloadEnd.Broadcast(true, AmmoNeedTake);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon1P && SkeletalMeshWeapon1P->GetAnimInstance())
	{
		SkeletalMeshWeapon1P->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon1P && !SkeletalMeshWeapon1P->SkeletalMesh)
	{
		SkeletalMeshWeapon1P->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::Fire_OnServer_Implementation()
{
	// char fire animation
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
	{
		AnimToPlay = WeaponSetting.AnimationWeaponInfo.AnimCharFireAiming;
	}
	else
	{
		AnimToPlay = WeaponSetting.AnimationWeaponInfo.AnimCharFire;
	}

	// weapon fire animation
	if (WeaponSetting.AnimationWeaponInfo.AnimWeaponFire)
	{
		AnimWeaponStart_Multicast(WeaponSetting.AnimationWeaponInfo.AnimWeaponFire);
	}

	// shell drop logic
	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropmeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.ImpulseRandomDirection, WeaponSetting.ShellBullets.PowerImpulse,
				WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}

	// main logic
	FireTimer = WeaponSetting.RateOfFire;
	AmmoInWeapon.Round--;
	ChangeDispersionByShot();
	if (AnimToPlay) OnWeaponFireStart.Broadcast(AnimToPlay);
	FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);
	NumberProjectile = GetNumberProjectileByShoot();

	if (ShootLocation)
	{
		FVector SpawnLocation = SkeletalMeshWeapon1P->GetSocketLocation("RifleFireSocket");
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();

		if (ShowDebug) UE_LOG(LogTemp, Warning, TEXT("Current number of projectile: %f"), NumberProjectile);

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)  //Shotgun
		{
			EndLocation = GetFireEndLocation();

			if (ShowDebug) UE_LOG(LogTemp, Warning, TEXT("Current end location: %f, %f, %f"), EndLocation.X, EndLocation.Y, EndLocation.Z);

			FVector Dir = EndLocation - SpawnLocation;
			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{
				// Projectile Init ballistic fire
				// rotate arrow comp to the camera dir
				FRotator rotatorArrowComp = UKismetMathLibrary::FindLookAtRotation(ShootLocation->GetForwardVector(), EndLocation);
				ShootLocation->SetWorldRotation(rotatorArrowComp);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				if (ShowDebug) UE_LOG(LogTemp, Warning, TEXT("Current bullet spawn location: %f, %f, %f"), SpawnLocation.X, SpawnLocation.Y, SpawnLocation.Z);
				if (ShowDebug) UE_LOG(LogTemp, Warning, TEXT("Current SkeletalMeshWeapon1P location: %f, %f, %f"), SkeletalMeshWeapon1P->GetComponentLocation().X, SkeletalMeshWeapon1P->GetComponentLocation().Y, SkeletalMeshWeapon1P->GetComponentLocation().Z);
				if (ShowDebug) UE_LOG(LogTemp, Warning, TEXT("Current SkeletalMeshWeapon3P location: %f, %f, %f"), SkeletalMeshWeapon3P->GetComponentLocation().X, SkeletalMeshWeapon3P->GetComponentLocation().Y, SkeletalMeshWeapon3P->GetComponentLocation().Z);

				AMasterProjectile* myProjectile = Cast<AMasterProjectile>(GetWorld()->
					SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));

				if (myProjectile)
				{
					//ToDo Init Projectile settings by id in table row(or keep in weapon table)
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
					//Projectile->BulletProjectileMovement->InitialSpeed = 2500.0f;

					if (WeaponSetting.AnimationWeaponInfo.AnimCharFire)
					{
						OnWeaponFireStart.Broadcast(WeaponSetting.AnimationWeaponInfo.AnimCharFire);
					}

				}
			}
			else
			{
				FHitResult Hit;
				TArray<AActor*> IgnoreActors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation,
					ETraceTypeQuery::TraceTypeQuery4, false, IgnoreActors, EDrawDebugTrace::None,
					Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

				if (ShowDebug)
				{
					TraceShowDebug_Multicast(SpawnLocation, ShootLocation, WeaponSetting.DistanceTrace);
				}

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfaceType))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfaceType];
						if (myMaterial && Hit.GetComponent()) SpawnTraceDecal_Multicast(myMaterial, Hit);
					}

					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfaceType))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfaceType];
						if (myParticle) SpawnTraceParticle_Multicast(myParticle, Hit);
					}

					if (WeaponSetting.ProjectileSetting.HitSound) SpawnTraceHitSound_Multicast(WeaponSetting.ProjectileSetting.HitSound, Hit);

					// UDefaultTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, mySurfaceType);

					/*if (Hit.GetActor()->GetClass()->ImplementsInterface(UTPS_InterfaceGameActor::StaticClass()))
					{
						ITPS_InterfaceGameActor::Execute_AvalibleForEffects(Hit.GetActor());
						ITPS_InterfaceGameActor::Execute_AvalibleForEffectsOnlyCPP(Hit.GetActor());
						ITPS_InterfaceGameActor::Execute_AvalibleForEffectsBP(Hit.GetActor());
					}*/

					// UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(Hit.GetActor(), FName("Effect"));

					UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
				}
			}
		}
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

int8 AWeaponDefault::GetNumberProjectileByShoot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::TraceShowDebug_Multicast_Implementation(FVector SpawnLocation, UArrowComponent* ShootLoc, float DistanceTrace)
{
	DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLoc->GetForwardVector() * DistanceTrace,
		FColor::Black, false, 5.0f, (uint16)'\000', 0.5f);
}

void AWeaponDefault::SpawnTraceDecal_Multicast_Implementation(UMaterialInterface* HitMaterial, FHitResult Hit)
{
	UGameplayStatics::SpawnDecalAttached(HitMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint,
		Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition);
}

void AWeaponDefault::SpawnTraceParticle_Multicast_Implementation(UParticleSystem* HitParticle, FHitResult Hit)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
}

void AWeaponDefault::SpawnTraceHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult Hit)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, Hit.ImpactPoint);
}

void AWeaponDefault::AnimWeaponStart_Multicast_Implementation(UAnimMontage* Anim)
{
	if (Anim && SkeletalMeshWeapon1P && SkeletalMeshWeapon1P->GetAnimInstance())
	{
		SkeletalMeshWeapon1P->GetAnimInstance()->Montage_Play(Anim);
	}
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CanWeaponFire()) WeaponFiring = bIsFire;
	else WeaponFiring = false;

	FireTimer = 0.01f;
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector FactEndLocation = FVector(0.0f);

	AMasterCharacter* myChar = Cast<AMasterCharacter>(GetOwner());

	if (myChar)
	{
		FHitResult HitTarget;
		TArray<AActor*> IgnoreActors;
		UKismetSystemLibrary::LineTraceSingle(GetWorld(), SkeletalMeshWeapon3P->GetSocketLocation("RifleFireSocket"), 
			(UKismetMathLibrary::GetForwardVector(MyPawn->GetController()->GetControlRotation()) * 20000.0f) + ShootLocation->GetComponentLocation(),
			ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors, EDrawDebugTrace::ForDuration,
			HitTarget, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

		FactEndLocation = HitTarget.ImpactPoint;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("Error: NO myChar")); }

		// debug
	if (ShowFireDebug) DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(),
			WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f,
			GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, 0.1f, (uint8)'\000', 1.0f);

	if (ShowFireDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() +
			ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.0f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), FactEndLocation, FColor::Purple, false, 5.0f, (uint8)'\000', 0.5f);
	}


	return FactEndLocation;
}

bool AWeaponDefault::CanWeaponFire()
{
	return !BlockFire;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	int8 AvailableAmmoForWeapon = 0;

	if (GetOwner())
	{
		UMasterInventoryComp* MyInv = Cast<UMasterInventoryComp>(GetOwner()->GetComponentByClass(UMasterInventoryComp::StaticClass()));

		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}

	return AvailableAmmoForWeapon;
}

void AWeaponDefault::SetOwningPawn(AMasterCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		SetInstigator(NewOwner);
		MyPawn = NewOwner;
		SetOwner(NewOwner);
	}
}

void AWeaponDefault::DetachMeshFromPawn()
{
	SkeletalMeshWeapon1P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	SkeletalMeshWeapon1P->SetHiddenInGame(true);

	SkeletalMeshWeapon3P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	SkeletalMeshWeapon3P->SetHiddenInGame(true);
}

void AWeaponDefault::AttachMeshToPawn()
{
	if (MyPawn)
	{
		DetachMeshFromPawn();

		FName AttachPoint = MyPawn->GetWeaponAttachPoint();
		if (MyPawn->IsLocallyControlled() == true)
		{
			USkeletalMeshComponent* PawnMesh1p = MyPawn->GetSpecifcPawnMesh(true);
			USkeletalMeshComponent* PawnMesh3p = MyPawn->GetSpecifcPawnMesh(false);
			SkeletalMeshWeapon1P->SetHiddenInGame(false);
			SkeletalMeshWeapon3P->SetHiddenInGame(false);

			if (PawnMesh1p) { SkeletalMeshWeapon1P->AttachToComponent(PawnMesh1p, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint); }
			else { UE_LOG(LogTemp, Warning, TEXT("Error: NO PawnMesh1p")); }
			if (PawnMesh3p) { SkeletalMeshWeapon3P->AttachToComponent(PawnMesh3p, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint); }
			else { UE_LOG(LogTemp, Warning, TEXT("Error: NO PawnMesh3p")); }

			SkeletalMeshWeapon1P->SetOnlyOwnerSee(true);
			SkeletalMeshWeapon3P->SetOwnerNoSee(true);
		}
		else
		{
			USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
			USkeletalMeshComponent* UsePawnMesh = MyPawn->GetPawnMesh();
			UseWeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);
			UseWeaponMesh->SetHiddenInGame(false);

			USkeletalMeshComponent* PawnMesh1p = MyPawn->GetSpecifcPawnMesh(true);
			SkeletalMeshWeapon1P->SetHiddenInGame(false);
			if (PawnMesh1p) { SkeletalMeshWeapon1P->AttachToComponent(PawnMesh1p, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint); }
			else { UE_LOG(LogTemp, Warning, TEXT("Error: NO PawnMesh1p")); }
			SkeletalMeshWeapon1P->SetOnlyOwnerSee(true);
		}
	}
}

USkeletalMeshComponent* AWeaponDefault::GetWeaponMesh() const
{
	return (MyPawn != NULL && MyPawn->IsFirstPerson()) ? SkeletalMeshWeapon1P : SkeletalMeshWeapon3P;
}

void AWeaponDefault::OnRep_MyPawn()
{
	if (MyPawn)
	{
		OnEnterInventory(MyPawn);
	}
	else
	{
		OnLeaveInventory();
	}
}

void AWeaponDefault::OnEnterInventory(AMasterCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
}

void AWeaponDefault::OnLeaveInventory()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		SetOwningPawn(NULL);
	}
}

bool AWeaponDefault::CanWeaponReload()
{
	bool result = true;

	if (GetOwner())
	{
		UMasterInventoryComp* MyInv = Cast<UMasterInventoryComp>(GetOwner()->GetComponentByClass(UMasterInventoryComp::StaticClass()));

		if (MyInv)
		{
			int8 AvailableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				result = false;
				MyInv->OnWeaponHaveNotRound.Broadcast(MyInv->GetWeaponIndexSlotByName(WeaponSetting.WeaponName));
			}
			else
			{
				MyInv->OnWeaponHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(WeaponSetting.WeaponName));
			}
		}
	}

	return result;
}

void AWeaponDefault::InitReload()
{
	//On server
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;

	UAnimMontage* FPAnimToPlay = nullptr;
	UAnimMontage* TPAnimToPlay = nullptr;

	if (WeaponAiming)
	{
		FPAnimToPlay = WeaponSetting.AnimationWeaponInfo.FPAnimCharReloadAiming;
		TPAnimToPlay = WeaponSetting.AnimationWeaponInfo.TPAnimCharReloadAiming;
	}
	else
	{
		FPAnimToPlay = WeaponSetting.AnimationWeaponInfo.FPAnimCharReload;
		TPAnimToPlay = WeaponSetting.AnimationWeaponInfo.TPAnimCharReload;
	}

	OnWeaponReloadStart.Broadcast(FPAnimToPlay, TPAnimToPlay);

	if (WeaponSetting.AnimationWeaponInfo.AnimWeaponReload
		&& SkeletalMeshWeapon1P
		&& SkeletalMeshWeapon1P->GetAnimInstance())
	{
		AnimWeaponStart_Multicast(FPAnimToPlay);
	}

	if (WeaponSetting.ClipDropMesh.DropMesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	// CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const { return CurrentDispersion; }

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
	float LifeTimeMesh, float ImpulseRundomDirection, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X +
			this->GetActorRightVector() * Offset.GetLocation().Y +
			this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

		ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRundomDirection, PowerImpulse, CustomMass, LocalDir);
	}
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset,
				WeaponSetting.ClipDropMesh.DropmeshImpulseDir, WeaponSetting.ClipDropMesh.DropMeshLifeTime,
				WeaponSetting.ClipDropMesh.ImpulseRandomDirection, WeaponSetting.ClipDropMesh.PowerImpulse,
				WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
		{
			DropClipTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropmeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.ImpulseRandomDirection, WeaponSetting.ShellBullets.PowerImpulse,
				WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
	float LifeTimeMesh, float ImpulseRundomDirection, float PowerImpulse, float CustomMass, FVector LocalDir)
{
	AStaticMeshActor* NewActor = nullptr;
	FActorSpawnParameters Param;

	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, Param);

	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		//set parametrs for new actor
		NewActor->SetActorTickEnabled(false);
		NewActor->InitialLifeSpan = LifeTimeMesh;
		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if (CustomMass > 0.0f)
		{
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
		}

		if (!DropImpulseDirection.IsNearlyZero())
		{
			FVector FinalDir;
			LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

			if (!FMath::IsNearlyZero(ImpulseRundomDirection))
			{
				FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRundomDirection);
			}

			FinalDir.GetSafeNormal(0.0001f);

			NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
		}
	}
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FxFire, USoundBase* SoundFire)
{
	if (SoundFire)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
	}

	if (FxFire)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxFire, ShootLocation->GetComponentTransform());
	}
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AWeaponDefault, AmmoInWeapon);
	DOREPLIFETIME(AWeaponDefault, WeaponReloading);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
}