// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterInventoryComp.h"
#include "MasterGameInstance.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UMasterInventoryComp::UMasterInventoryComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);

	// ...
}


// Called when the game starts
void UMasterInventoryComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMasterInventoryComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UMasterInventoryComp::SwitchWeaponToIndexNOP(int32 ChangeToIndex, int32 OldIndex, FAmmoInWeapon OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;

	if (ChangeToIndex > WeaponSlots.Num() - 1) CorrectIndex = 0;
	else { if (ChangeToIndex < 0) CorrectIndex = WeaponSlots.Num() - 1; }

	FName NewIdWeapon;
	FAmmoInWeapon NewAmmoInWeaponInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AmmoInWeapon.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UMasterGameInstance* myGI = Cast<UMasterGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAmmoInWeaponInfo = WeaponSlots[CorrectIndex].AmmoInWeapon;
			}
		}
	}

	if (!bIsSuccess)
	{
		int8 ForwardIteration = 0;
		int8 BackIteration = 0;
		int8 tmpIndex = 0;
		while (ForwardIteration < WeaponSlots.Num() && !bIsSuccess)
		{
			ForwardIteration++;

			if (bIsForward)
			{
				//Seconditeration = 0;

				tmpIndex = ChangeToIndex + ForwardIteration;
			}
			else
			{
				BackIteration = WeaponSlots.Num() - 1;

				tmpIndex = ChangeToIndex - ForwardIteration;
			}

			if (WeaponSlots.IsValidIndex(tmpIndex))
			{
				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[tmpIndex].AmmoInWeapon.Round > 0)
					{
						//WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
						NewAmmoInWeaponInfo = WeaponSlots[tmpIndex].AmmoInWeapon;
						NewCurrentIndex = tmpIndex;
					}
					else
					{
						FWeaponInfo myInfo;
						UMasterGameInstance* myGI = Cast<UMasterGameInstance>(GetWorld()->GetGameInstance());

						myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAmmoInWeaponInfo = WeaponSlots[tmpIndex].AmmoInWeapon;
								NewCurrentIndex = tmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				//go to end of LEFT of array weapon slots
				if (OldIndex != BackIteration)
				{
					if (WeaponSlots.IsValidIndex(BackIteration))
					{
						if (!WeaponSlots[BackIteration].NameItem.IsNone())
						{
							if (WeaponSlots[BackIteration].AmmoInWeapon.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[BackIteration].NameItem;
								NewAmmoInWeaponInfo = WeaponSlots[BackIteration].AmmoInWeapon;
								NewCurrentIndex = BackIteration;
							}
							else
							{
								FWeaponInfo myInfo;
								UMasterGameInstance* myGI = Cast<UMasterGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[BackIteration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[BackIteration].NameItem;
										NewAmmoInWeaponInfo = WeaponSlots[BackIteration].AmmoInWeapon;
										NewCurrentIndex = BackIteration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					//go to same weapon when start
					if (WeaponSlots.IsValidIndex(BackIteration))
					{
						if (!WeaponSlots[BackIteration].NameItem.IsNone())
						{
							if (WeaponSlots[BackIteration].AmmoInWeapon.Round > 0)
							{
								//WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UMasterGameInstance* myGI = Cast<UMasterGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[BackIteration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Count > 0)
										{
											//WeaponGood, it same weapon do nothing
										}
										else
										{
											//Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					BackIteration++;
				}
				else
				{
					BackIteration--;
				}

			}
		}
	}
	if (bIsSuccess)
	{
		SetAmmoInWeaponInfo(OldIndex, OldInfo);
		SwitchWeaponEvent_OnServer(NewIdWeapon, NewAmmoInWeaponInfo, NewCurrentIndex, AmmoSlots[NewCurrentIndex]);
	}

	return bIsSuccess;
}

void UMasterInventoryComp::SetAmmoInWeaponInfo(int32 IndexWeapon, FAmmoInWeapon NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon) //WeaponSlots[i].IndexSlot
			{
				WeaponSlots[i].AmmoInWeapon = NewInfo;
				bIsFind = true;

				WeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalWeaponInfo No found weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalWeaponInfo - No correct index weapon - %d"), IndexWeapon);
	}
}

int32 UMasterInventoryComp::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

bool UMasterInventoryComp::ChekCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
		{
			result = true;
		}
		i++;
	}
	return result;
}

void UMasterInventoryComp::WeaponAmmoAvalibleEvent_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnWeaponAmmoAvalible.Broadcast(TypeWeapon);
}

void UMasterInventoryComp::WeaponAmmoEmptyEvent_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
}

void UMasterInventoryComp::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeWeapon, int32 Count)
{
	OnAmmoChange.Broadcast(TypeWeapon, Count);
}

bool UMasterInventoryComp::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;

			AvailableAmmoForWeapon = AmmoSlots[i].MaxCount;

			if (AmmoSlots[i].MaxCount > 0)
			{
				return true;
			}
		}
		i++;
	}

	if (AvailableAmmoForWeapon <= 0)
	{
		WeaponAmmoEmptyEvent_Multicast(TypeWeapon);
	}
	else
	{
		WeaponAmmoAvalibleEvent_Multicast(TypeWeapon);
	}

	return false;
}

void UMasterInventoryComp::AmmoSlotChangeAmmo(EWeaponType WeaponType, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == WeaponType)
		{
			AmmoSlots[i].MaxCount += CoutChangeAmmo; // minus cout input
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount) { AmmoSlots[i].Count = AmmoSlots[i].MaxCount; }
			UE_LOG(LogTemp, Warning, TEXT("Max Count - %i"), AmmoSlots[i].MaxCount);
			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].MaxCount);
			bIsFind = true;
		}
		i++;
	}
}

void UMasterInventoryComp::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;

	MaxSlotWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AmmoInWeapon, 0, AmmoSlots[0]);
		}
	}
}

void UMasterInventoryComp::SwitchWeaponEvent_OnServer_Implementation(FName WeapomName, FAmmoInWeapon AdditionalInfo, int32 IndexSlot, FAmmoSlot AmmoSlotInfo)
{
	OnSwitchWeapon.Broadcast(WeapomName, AdditionalInfo, IndexSlot, AmmoSlotInfo);
}

void UMasterInventoryComp::WeaponAdditionalInfoChangeEvent_Multicast_Implementation(int32 IndexSlot, FAmmoInWeapon AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UMasterInventoryComp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UMasterInventoryComp, WeaponSlots);
	DOREPLIFETIME(UMasterInventoryComp, AmmoSlots);
}