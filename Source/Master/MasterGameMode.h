// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MasterPlayerController.h"
#include "MasterGameMode.generated.h"

UCLASS(minimalapi)
class AMasterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMasterGameMode();

UFUNCTION()
	void TryToRespawnPlayer(AMasterPlayerController* MasterPlayerController);
};



