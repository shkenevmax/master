// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterPlayerController.h"

AMasterPlayerController::AMasterPlayerController()
{

}

void AMasterPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}

void AMasterPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AMasterCharacter* MyCharacter = Cast<AMasterCharacter>(InPawn);
	if (MyCharacter)
	{
		MyCharacter->MyPlayerController = this;
	}
}

TMap<FString, int> AMasterPlayerController::MapValueSort(TMap<FString, int> MapName, bool ascending)
{
	if (ascending)
	{
		MapName.ValueSort([](const int& A, const int& B) {return A < B; });
	}
	else
	{
		MapName.ValueSort([](const int& A, const int& B) {return A > B; });
	}

	return MapName;
}