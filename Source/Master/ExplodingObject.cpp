// Fill out your copyright notice in the Description page of Project Settings.

#include "ExplodingObject.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AExplodingObject::AExplodingObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ExploadingStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterMesh1P"));
	ExploadingStaticMesh->SetupAttachment(RootComponent);
	ExploadingStaticMesh->bCastDynamicShadow = true;
	ExploadingStaticMesh->CastShadow = true;
}

// Called when the game starts or when spawned
void AExplodingObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExplodingObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AExplodingObject::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 1.0f) { Explose_OnServer(EventInstigator); }
	return ActualDamage;
}

void AExplodingObject::Explose_OnServer_Implementation(AController* EventInstigator)
{
	Explode_Multicast(ExplodeFX, ExplodeSound, DamageOuterRadius, DamageInnerRadius, 1);

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ExplodeMaxDamage,
		ExplodeMinDamage,
		GetActorLocation(),
		DamageInnerRadius,
		DamageOuterRadius,
		5,
		NULL, IgnoredActor, this, EventInstigator);

	this->Destroy();
}

void AExplodingObject::Explode_Multicast_Implementation(UParticleSystem* MulticastExplodeFX, USoundBase* MulticastExplodeSound, float MaxDebugRadius, float MinDebugRadius, int DebugExplode)
{
	if (DebugExplode)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), MinDebugRadius, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), MaxDebugRadius, 12, FColor::Red, false, 12.0f);
	}

	if (MulticastExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MulticastExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (MulticastExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), MulticastExplodeSound, GetActorLocation());
	}
}