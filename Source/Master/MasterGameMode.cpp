// Copyright Epic Games, Inc. All Rights Reserved.

#include "MasterGameMode.h"
#include "MasterHUD.h"
#include "MasterCharacter.h"
#include "MasterPlayerController.h"
#include "UObject/ConstructorHelpers.h"

AMasterGameMode::AMasterGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	// static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/Character/FirstPersonCharacter"));
	// DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMasterHUD::StaticClass();
}

void AMasterGameMode::TryToRespawnPlayer(AMasterPlayerController* MasterPlayerController)
{
	int32 StartPointNum = FMath::RandRange(1,4);
	FString StartPointName;
	switch (StartPointNum)
	{
	case 1: StartPointName = "Start1"; break;
	case 2: StartPointName = "Start2"; break;
	case 3: StartPointName = "Start3"; break;
	case 4: StartPointName = "Start4"; break;
	default: StartPointName = "Start"; break;
		break;
	}
	RestartPlayerAtPlayerStart(MasterPlayerController, FindPlayerStart(MasterPlayerController, StartPointName));
}
