// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterHealthComp.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UMasterHealthComp::UMasterHealthComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UMasterHealthComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMasterHealthComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UMasterHealthComp::GetCurrentHealth()
{
	return Health;
}

void UMasterHealthComp::SetCurrentHealth(float NewHelth)
{
	Health = NewHelth;
}

bool UMasterHealthComp::GetIsAlive()
{
	return bIsAlive;
}

void UMasterHealthComp::SetMaxHealth(float NewHealthValue)
{
	Health = NewHealthValue;
}

void UMasterHealthComp::DeadEvent_Multicast_Implementation(AController* DamageInstigator)
{
	OnDead.Broadcast(DamageInstigator);
}

void UMasterHealthComp::HealthChangeEvent_Multicast_Implementation(float HealthPoints, float Value)
{
	OnOnHealthChange.Broadcast(HealthPoints, Value);
}

void UMasterHealthComp::ChangeCurrentHealth_OnServer_Implementation(float ChangeValue, AController* DamageInstigator)
{
	if (bIsAlive)
	{
		ChangeValue *= CoefDamage;
		Health += ChangeValue;

		HealthChangeEvent_Multicast(Health, ChangeValue);

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health < 0.0f)
			{
				bIsAlive = false;
				DeadEvent_Multicast(DamageInstigator);
				Health = 0.0f;
			}
		}
	}
}

void UMasterHealthComp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UMasterHealthComp, Health);
	DOREPLIFETIME(UMasterHealthComp, bIsAlive);
}