// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterCharHealthComp.h"

void UMasterCharHealthComp::ChangeCurrentHealth_OnServer(float ChangeValue, AController* DamageInstigator)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
			UE_LOG(LogTemp, Warning, TEXT("UTPSCharHealthComponent::ChangeCurrentHealth Shield < 0"));
		}
	}
	else
	{
		Super::ChangeCurrentHealth_OnServer(ChangeValue, DamageInstigator);
	}
}

float UMasterCharHealthComp::GetCurrentShield()
{
	return Shield;
}

void UMasterCharHealthComp::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	ShieldChange_Multicast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldCoolDownTimer, this, &UMasterCharHealthComp::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
	}
}

void UMasterCharHealthComp::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryRateTimer, this, &UMasterCharHealthComp::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UMasterCharHealthComp::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	ShieldChange_Multicast(Shield, ShieldRecoveryValue);
}

float UMasterCharHealthComp::GetShieldValue()
{
	return Shield;
}

void UMasterCharHealthComp::ShieldChange_Multicast_Implementation(float ShieldPoints, float Damage)
{
	OnShieldChange.Broadcast(ShieldPoints, Damage);
}
