// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MasterHealthComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDead, AController*, DamageInstigator);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MASTER_API UMasterHealthComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMasterHealthComp();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnOnHealthChange OnOnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
		float Health = 100.0f;
	UPROPERTY(Replicated)
		bool bIsAlive = true;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CoefDamage = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(float NewHelth);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
		virtual void ChangeCurrentHealth_OnServer(float ChangeValue, AController* DamageInstigator);
	UFUNCTION(BlueprintCallable, Category = "Health")
		bool GetIsAlive();

	void SetMaxHealth(float NewHealthValue);

	UFUNCTION(NetMulticast, Reliable)
		void HealthChangeEvent_Multicast(float HealthPoints, float Value);
	UFUNCTION(NetMulticast, Reliable)
		void DeadEvent_Multicast(AController* DamageInstigator);
};
