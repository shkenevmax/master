// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplodingObject.generated.h"

UCLASS()
class MASTER_API AExplodingObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplodingObject();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* ExploadingStaticMesh = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplodeSettings")
		float DamageInnerRadius = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplodeSettings")
		float DamageOuterRadius = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplodeSettings")
		UParticleSystem* ExplodeFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplodeSettings")
		USoundBase* ExplodeSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplodeSettings")
		float ExplodeMaxDamage = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplodeSettings")
		float ExplodeMinDamage = 10.0f;

	UFUNCTION(Server, Reliable)
		void Explose_OnServer(AController* EventInstigator);
	UFUNCTION(NetMulticast, Reliable)
		void Explode_Multicast(UParticleSystem* MulticastExplodeFX, USoundBase* MulticastExplodeSound, float MaxDebugRadius, float MinDebugRadius, int DebugExplode);

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};
