// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DefaultTypes.h"
#include "MasterProjectile.generated.h"

UCLASS()
class MASTER_API AMasterProjectile : public AActor
{
	GENERATED_BODY()

public:
	AMasterProjectile();

	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
		class USphereComponent* CollisionComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
		class UProjectileMovementComponent* ProjectileMovement;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UParticleSystemComponent* BulletFX = nullptr;
	UPROPERTY(BlueprintReadOnly)
		FProjectileInfo ProjectileSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void InitProjectile(FProjectileInfo InitParam);
	UFUNCTION()
		virtual void ImpactProjectile();

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	UFUNCTION(NetMulticast, Reliable)
		void InitVisualMeshProjectile_Multicast(UStaticMesh* NewTemplate, FTransform TemplateRelative);
	UFUNCTION(NetMulticast, Reliable)
		void InitVisualTrailProjectile_Multicast(UParticleSystem* NewTemplate, FTransform TemplateRelative);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnHitDecal_Multicast(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnHitFX_Multicast(UParticleSystem* FxTemplate, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
		void InitVelosity_Multicast(float InitSpeed, float MaxSpeed);
};

