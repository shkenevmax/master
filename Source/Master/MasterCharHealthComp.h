// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MasterHealthComp.h"
#include "MasterCharHealthComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class MASTER_API UMasterCharHealthComp : public UMasterHealthComp
{
	GENERATED_BODY()

public:	

	FTimerHandle ShieldCoolDownTimer;
	FTimerHandle ShieldRecoveryRateTimer;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

protected:

	float Shield = 100.0f;

public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;
		
	void ChangeCurrentHealth_OnServer(float ChangeValue, AController* DamageInstigator) override;

	float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();

	UFUNCTION(BlueprintCallable)
		float GetShieldValue();

	UFUNCTION(NetMulticast, Reliable)
		void ShieldChange_Multicast(float ShieldPoints, float Damage);
};
