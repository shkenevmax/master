// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MasterCharacter.h"
#include "MasterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MASTER_API AMasterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMasterPlayerController();

protected:
	
	virtual void OnUnPossess() override;
	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION(BlueprintCallable)
	TMap<FString, int> MapValueSort(TMap<FString, int> MapName, bool ascending = false);
};
